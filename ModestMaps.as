﻿package {
    import com.modestmaps.Map;
    import com.modestmaps.TweenMap;
    import com.modestmaps.core.MapExtent;
    import com.modestmaps.events.MapEvent;
    import com.modestmaps.events.MarkerEvent;
    import com.modestmaps.extras.MapControls;
	import com.modestmaps.overlays.PolygonClip;
	import com.modestmaps.overlays.PolygonMarker;
    import com.modestmaps.extras.ZoomSlider;
	import com.modestmaps.extras.MapCopyright;
    import com.modestmaps.geo.Location;
    import com.modestmaps.mapproviders.*;
    import com.modestmaps.mapproviders.microsoft.*;
		
	import flash.display.MovieClip;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.text.TextField;
    import flash.text.TextFormat;    
	import fl.data.DataProvider;
	import flash.errors.*;
	import flash.events.*;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.net.Responder;
	import fl.controls.Button;
	import fl.controls.DataGrid; 
	import fl.events.*;
	import flash.events.*;
	import flash.net.NetConnection;
	import flash.net.Responder;	
	
	public class ModestMaps extends MovieClip {
		//variables webService
		private var gateway:String = "http://www.montech.mobi/Servy/gateway.php";;
		private var conect:NetConnection = new NetConnection;
		private var responder:Responder;
		private var Con:String = new String();
		private var WebSer:String = "Montech"; 
		public var WebSerCon:String = WebSer+".Consulta";
		public var WebSerMod:String = WebSer+".Modificacion";
		public var WebSerHac:String = WebSer+".Hacking";
		public var WebSerLog:String = WebSer+".Login";
		public var WebSerMod2:String = WebSer+".Moding";
		public var marker:SampleMarker = new SampleMarker();
		public var loc:Location;
		public var i:int=new int();
		public var mark:Array = new Array();
		public var locmark:Array = new Array();

		//public var polygonClip:PolygonClip = new PolygonClip(map);
		
		//variables MAPA
        private var map:Map;
        // informacion sobre el marker 
        private var tooltip:Tooltip; 
        // status text field at bottom of screen
        private var status:TextField;     
        // botones de tipos de mapas
        private var mapButtons:MovieClip;        
        // relleno alrededor mapa en píxeles
        private const PADDING:int = 19;
        
        /**
         * Este constructor es llamado automáticamente cuando se inicia el SWF
         */
        public function ModestMaps()
        {		
			conect.connect(gateway); 
        	// setup stage
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            stage.addEventListener(Event.RESIZE, onResize);	            	   
            
            // create child components
            createChildren();
			onResize(); 
			ConsultaWebService("SELECT nom, lat, lng, temp, lts FROM  `Ganado` WHERE idpre=1 ORDER BY id ASC LIMIT 0 , 30", WebSerCon, res_busMarker);
                  		
        }
		
		private function createChildren():void 
        {			
			// create map
            map = new TweenMap(stage.stageWidth - 2 * PADDING, stage.stageHeight - 2 * PADDING, 
            				   true,
            				   new MicrosoftProvider("AERIAL",false,1,19),
							   new MapExtent(-40.960051744842056, -40.44051744842056, -73.04364852905274, -73.04364852905274));
								
            map.addEventListener(MouseEvent.DOUBLE_CLICK, map.onDoubleClick);
			map.addEventListener(MouseEvent.CLICK, mapClick);

            map.x = map.y = PADDING;
	        // listen for map events
            map.addEventListener(MapEvent.ZOOMED_BY, onZoomed);
            map.addEventListener(MapEvent.STOP_ZOOMING, onStopZoom);
            map.addEventListener(MapEvent.PANNED, onPanned);
            map.addEventListener(MapEvent.STOP_PANNING, onStopPan);
            map.addEventListener(MapEvent.RESIZED, onResized);
			//map.addEventListener(MapEvent.CLICK, mapClick)
				        
            // listen for marker events
            /* map.addEventListener(MarkerEvent.MARKER_CLICK, Callback.create(onMarkerClick));
            map.addEventListener(MarkerEvent.MARKER_ROLL_OVER, onMarkerRollOver);
            map.addEventListener(MarkerEvent.MARKER_ROLL_OUT, onMarkerRollOut);*/
	   
            // add some controls using the MapControls extra
            // we're adding them as children of map so they move with the map
            map.addChild(new MapControls(map));	
            map.addChild(new ZoomSlider(map));			
			//map.addChild(new MapCopyright(map));
			
	        // create tooltip
	        tooltip = new Tooltip();
	        
            // create text field to hold status text	                
            status = new TextField();
            status.defaultTextFormat = new TextFormat('Verdana', 10, 0xeeeeee);
            status.selectable = false;
            status.text = 'Bienvenido a MONTECH - Sistema de Gestion Agropecuaria...';
            status.width = 600;
            status.height = 20;
	        
            // create some provider buttons   
            mapButtons = new MovieClip();
            mapButtons.addChild(new MapProviderButton('Map Road', new MicrosoftRoadMapProvider()));
            mapButtons.addChild(new MapProviderButton('Map Aerial', new MicrosoftAerialMapProvider()));
            mapButtons.addChild(new MapProviderButton('Map Hybrid', new MicrosoftHybridMapProvider()));        	
            mapButtons.addChild(new MapProviderButton('Open Street Map', new OpenStreetMapProvider()));
			
            // arrange buttons 20px apart
			for( var n:int = 0;n < mapButtons.numChildren; n++) {
                mapButtons.getChildAt(n).y = n * 20;
            }
            
            // listen for map provider button clicks	      
            mapButtons.addEventListener(MouseEvent.CLICK, onProviderButtonClick);
			
			/////________________________POLIGONOS_________________________________________________________________________________________________________
			var polygonClip:PolygonClip = new PolygonClip(map);
			var locations:Array = [ new Location(-40.564355315219, -73.14851942062),
			new Location(-40.561463695881, -73.14978542327),
			new Location(-40.557573806083, -73.134786510),
			new Location(-40.559350758312, -73.134679222),
			new Location(-40.562692604919, -73.141030693),
			new Location(-40.566392894068, -73.1454295158)];
			
			
			var polygon:PolygonMarker = new PolygonMarker(map,locations,true); 
			polygonClip.attachMarker(polygon,polygon.location);			
			map.setExtent(MapExtent.fromLocations(locations));
			
var polygonClip2:PolygonClip = new PolygonClip(map);
			var locations2:Array = [ new Location(-40.558128089821, -73.1513732910),
			new Location(-40.554362130565, -73.15077247617),
			new Location(-40.555226200548,-73.13997926712),
			new Location(-40.557557503550, -73.13474359512),
			new Location(-40.559578987291, -73.1425327301),
			new Location(-40.561463695881, -73.14978542327)];
			
			var polygon2:PolygonMarker = new PolygonMarker(map,locations2,true); 
			polygonClip2.attachMarker(polygon2, polygon2.location);
			map.setExtent(MapExtent.fromLocations(locations2));
			

			
			/*var polygonClip:PolygonClip = new PolygonClip(map);
			var locations:Array = [ new Location(-40.564355315219, -73.14851942062),
			new Location(-40.561463695881, -73.14978542327),
			new Location(-40.557573806083, -73.134786510),
			new Location(-40.559350758312, -73.134679222),
			new Location(-40.562692604919, -73.141030693),
			new Location(-40.566392894068, -73.1454295158)];
			
			var polygon:PolygonMarker = new PolygonMarker(map,locations,true); 
			polygonClip.attachMarker(polygon, polygon.location);			
			map.setExtent(MapExtent.fromLocations(locations));*/
			
			
			/*var polygonClip2:PolygonClip = new PolygonClip(map);
			var locations2:Array = [ new Location(-40.558128089821, -73.1513732910),
			new Location(-40.554362130565, -73.15077247617),
			new Location(-40.555226200548,-73.13997926712),
			new Location(-40.557557503550, -73.13474359512),
			new Location(-40.559578987291, -73.1425327301),
			new Location(-40.561463695881, -73.14978542327)];
			
			var polygon2:PolygonMarker = new PolygonMarker(map,locations2,true); 
			polygonClip2.attachMarker(polygon2, polygon2.location);
			map.setExtent(MapExtent.fromLocations(locations2));*/
				
            // add children to the display list
			addChild(map);
            addChild(status);
            addChild(mapButtons);	  
            addChild(tooltip); 
			
			addChild(polygonClip);
			addChild(polygonClip2);
		}
       
        /**
         * Consultas al WebService.............
         */		
        public function ConsultaWebService(Con,webserFunction,res_Con:Function):void{		
			responder = new Responder(res_Con, error);
			conect.call(webserFunction, responder, Con);			
		}
		
		public function error(error:Object):void {
			//trace("Flash Error: " + error.description);
		}
        
private function res_busMarker(resultado:Object):void{
	if(resultado){
		for(i=0;i<resultado.length;i++){
			mark.push("marker"+resultado[i][0]);
			locmark.push("locmarker"+resultado[i][0]);
			this.mark[i] = new SampleMarker();
			this.mark[i].name = "marker"+resultado[i][0];
			this.mark[i].title = "INFO: "+ 'IdTag: '+resultado[i][0]+' - Temp: '+resultado[i][3]+'       Litros: '+resultado[i][4];	
			this.locmark[i] = new Location(resultado[i][1], resultado[i][2]);
			map.putMarker(this.locmark[i], this.mark[i]);
			this.mark[i].addEventListener(MouseEvent.CLICK, Callback.create(MarClick,  this.mark[i].name, this.locmark[i].lat, this.locmark[i].lon));
			map.addEventListener(MarkerEvent.MARKER_CLICK, Callback.create(onMarkerClick, i));
            map.addEventListener(MarkerEvent.MARKER_ROLL_OVER, Callback.create(onMarkerRollOver, i));
            map.addEventListener(MarkerEvent.MARKER_ROLL_OUT, Callback.create(onMarkerRollOut, i));
			this.mark[i].addEventListener(MouseEvent.MOUSE_DOWN, Callback.create(arrastarMark, i));
			this.mark[i].addEventListener(MouseEvent.MOUSE_UP, Callback.create(soltarMark, i));
		}
	}else{
		trace("error");
	}
}

		
		
        /**
         * Etapa controlador de cambio de tamaño
         */
        private function onResize(event:Event = null):void 
        {
			//map.setCenter(new Location(-40.44051744842056, -73.04364852905274));
			map.setRotation(0,null);
			map.setZoom(15);
            var w:Number = stage.stageWidth - 2 * PADDING;
            var h:Number = stage.stageHeight - 2 * PADDING;
	    	
            // position and size the map
            map.x = map.y = PADDING;
            map.setSize(w, h);
			
            // align the buttons to the right
            mapButtons.x = map.x + w - 120;
            mapButtons.y = map.y + 10;
	
            // place status just below the map on the left
            status.width = w;
            status.x = map.x + 2;
            status.y = map.y + h;
        }
		
        /**
         * Change map provider when provider buttons are clicked
         */
        private function onProviderButtonClick(event:Event):void 
        {
            var button:MapProviderButton = event.target as MapProviderButton;
            map.setMapProvider(button.mapProvider);
        }
        
		private function mapClick(event:MouseEvent):void{ 
			var ptmap:Point = new Point(10, 50);
			//ptmap = event.ptmap as Point(10, 50);
			
			//e=e.location;
			//var ptmap:Point = map.locationPoint(location.lat);
			//trace(ptmap.x+"  "+ptmap.y);
		}
		
		public function arrastarMark(e:MouseEvent,i){
			this.mark[i].startDrag();
		}
		public function soltarMark(e:MouseEvent,i){
			this.mark[i].stopDrag();
			//trace(getMarkerLocation(this.mark[i]));
		}
		// Marker Click
		public function onMarkerClick(e:MarkerEvent, i:Number):void 
		{
		
			var marker0 = new SampleMarker();
			marker0 = e.marker as SampleMarker;
			status.text = "Marker Clicked:  "+ e.currentTarget.name+ "  " + marker0.title + " - Posicion: " + e.txt;
			
		}
		
		
		//MARKER ONROLLOVER
		public function onMarkerRollOver(event:MarkerEvent,i:Number):void 
		{
			//event.marknom = SampleMarker;
			this.mark[i] = new SampleMarker();
			this.mark[i] = event.marker as SampleMarker;
			var pt:Point = map.locationPoint( event.location, this );
			tooltip.x = pt.x;
			tooltip.y = pt.y;
			tooltip.label = mark[i].title;
			tooltip.visible = true;
		}
		
		//MARKER ROLLOUT
		public function onMarkerRollOut(event:MarkerEvent, i:Number):void 
		{
			tooltip.visible = false;
		}
				
		public function MarClick(event:MouseEvent, mk:String, lat:Number, lon:Number):void{
			trace(mk+" - "+lat+ " - " +lon);
		}
		   
   /*     private function onMarkerClick(event:MarkerEvent, marknom:SampleMarker):void 
        {
			marknom = event.marknom as SampleMarker;
            status.text = "Marker Clicked:  " + marknom.title + " - Posicion: " + event.location;
			//trace(event.location.lat);
	    }
		
        private function onMarkerRollOver(event:MarkerEvent,marknom:SampleMarker):void 
        {
          marknom = event.marknom as SampleMarker;
            var pt:Point = map.locationPoint( event.location, this );
			tooltip.x = pt.x;
            tooltip.y = pt.y;
            tooltip.label = marknom.title;
            tooltip.visible = true;
        }
        
        private function onMarkerRollOut(event:MarkerEvent, marknom:SampleMarker):void 
        {
            tooltip.visible = false;
        }*/
        
        //---------------------
        // Map Event Handlers
        //---------------------
        private function onPanned(event:MapEvent):void 
        {  
			status.text = 'Panned by ' + event.panDelta.toString() + ', top left: ' + map.getExtent().northWest.toString() + ', bottom right: ' + map.getExtent().southEast.toString();
        }
        
        private function onStopPan(event:MapEvent):void 
        {
            status.text = 'Stopped panning, top left: ' + map.getExtent().northWest.toString() + ', center: ' + map.getCenterZoom()[0].toString() + ', bottom right: ' + map.getExtent().southEast.toString() + ', zoom: ' + map.getCenterZoom()[1];
        }
        
        private function onZoomed(event:MapEvent):void 
        {
            status.text = 'Zoomed by ' + event.zoomDelta.toFixed(3) + ', top left: ' + map.getExtent().northWest.toString() + ', bottom right: ' + map.getExtent().southEast.toString();
        }
        
        private function onStopZoom(event:MapEvent):void 
        {
            status.text = 'Stopped zooming, top left: ' + map.getExtent().northWest.toString() + ', center: ' + map.getCenterZoom()[0].toString() + ', bottom right: ' + map.getExtent().southEast.toString() + ', zoom: ' + map.getCenterZoom()[1];
        }
        
        private function onResized(event:MapEvent):void 
        {
            status.text = 'Resized to: ' + event.newSize[0] + ' x ' + event.newSize[1];
        }
    }
}
